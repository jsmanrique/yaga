from . import github, gitlab, grimoire

from scope.models import Project as ProjectScope, GitRepo, GitHubOrg, GitLabSet
from data.models import Project, Repository, Item

import django_rq

def from_github(name, pid):

    project = ProjectScope.objects.get(pk=pid)

    repos = github.get_repos(name)
    for repo_url in repos:
        git_repo, created = GitRepo.objects.get_or_create(url=repo_url, project=project)

def from_gitlab(name, pid):

    project = ProjectScope.objects.get(pk=pid)

    repos = gitlab.get_repos(name)
    for repo_url in repos:
        git_repo, created = GitRepo.objects.get_or_create(url=repo_url, project=project)

def update_data():

    for project in ProjectScope.objects.all():
        data_project, created = Project.objects.get_or_create(name=project.name)
    
    for repo in GitRepo.objects.all():
        django_rq.enqueue(grimoire.update_git_data_repo, repo.url, repo.project.name)