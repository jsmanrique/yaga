#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Python script to get a the list of git repos for a given Gitlab
# username or group name
#
# Copyright (C) 2020 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>


import requests

import logging

import urllib

import argparse
import configparser

import json

BASE_GITLAB_API_URL = 'https://gitlab.com/api/v4'

def parse_args(args):

    parser = argparse.ArgumentParser(description = 'Get a json file with the list of git repositories for a given GitLab username or groupname')
    parser.add_argument('-n', '--name', dest = 'gname', required = True, help = 'GitLab username or groupname')
    parser.add_argument('-a', '--api', dest = 'base_api_url', default=BASE_GITLAB_API_URL, help = 'GitLab instance API base url')

    parser.add_argument(
        '-d', '--debug',
        help="Print lots of debugging statements",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="Be verbose",
        action="store_const", dest="loglevel", const=logging.INFO,
    )

    return parser.parse_args()

def main(args):
    
    args = parse_args(args)

    repos = get_repos(args.gname, args.base_api_url)
    
    with open('{}_gitlab_repos.json'.format(args.gname), 'w') as f:
        json.dump({'git':repos}, f)

def get_gitlab_subgroups(gname, api_url = BASE_GITLAB_API_URL):

    sgroups = []

    page = 1
    per_page = 100

    s = requests.get(api_url+'/groups/{}/subgroups?page={}&per_page={}'.format(gname,page,per_page))
    
    subgroups = s.json()

    while len(subgroups) > 0:
        for subgroup in subgroups:
            logging.info('Found {}'.format(subgroup['full_path']))
            sgroups += get_gitlab_subgroups(urllib.parse.quote(subgroup['full_path'], safe=''))
            sgroups.append(subgroup['full_path'])
        page += 1
        s = requests.get(api_url+'/groups/{}/subgroups?page={}&per_page={}'.format(gname,page,per_page))
        subgroups = s.json()
    
    logging.info('Returning {} groups from {}: {}'.format(len(sgroups), gname, sgroups))

    return sgroups


def get_repos(gname, api_url = BASE_GITLAB_API_URL):

    r = requests.get(api_url+'/users?username={}'.format(gname))

    if len(r.json()) > 0:
        logging.info('{} is a GitLab user'.format(gname))
        query = '/users/{}'.format(gname)
        repos = get_repos_from_query(query)
    
    else:

        r = requests.get(api_url+'/groups/{}'.format(gname))

        if 'message' in r.json().keys():
            if r.json()['message'] == '404 Group Not Found':
                logging.info('{} is not a GitLab group')
                repos = []
        else:
            logging.info('{} is a GitLab group'.format(gname))
            query = '/groups/{}'.format(gname)
            repos = get_repos_from_query(query)

            groups = get_gitlab_subgroups(gname)

            for group in groups:
                repos += get_repos_from_query('/groups/'+urllib.parse.quote(group, safe=''))
    
    logging.info('Returning {} git repositories from {}: {}'.format(len(repos), gname, repos))
    return repos


def get_repos_from_query(gname, api_url = BASE_GITLAB_API_URL):

    page = 1
    per_page = 100
    repos = []

    r = requests.get(api_url+'{}/projects?page={}&per_page={}'.format(gname,page,per_page))

    projects = r.json()
    
    while len(projects) > 0:
        for project in projects:
            repos.append(project['http_url_to_repo'])
        page += 1
        r = requests.get(api_url+'{}/projects?page={}&per_page={}'.format(gname,page,per_page))
        projects = r.json()
    
    logging.info('Returning {} git repositories from {}: {}'.format(len(repos), gname, repos))
    return repos

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
