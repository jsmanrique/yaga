from django.urls import path
from etl import views

urlpatterns = [
    path('', views.update, name='update')
]