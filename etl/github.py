#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Python script to get a the list of git repos for a given GitHub
# username or group name
#
# Copyright (C) 2020 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>


import requests

import logging

import time

import argparse
import configparser

import json

BASE_GITHUB_API_URL = 'https://api.github.com'

def parse_args(args):

    parser = argparse.ArgumentParser(description = 'Get a json file with the list of git repositories for a given GitHub username or groupname')
    parser.add_argument('-n', '--name', dest = 'gname', required = True, help = 'GitLab username or groupname')
    parser.add_argument('-a', '--api', dest = 'base_api_url', default=BASE_GITHUB_API_URL, help = 'GitHub instance API base url')

    parser.add_argument(
        '-d', '--debug',
        help="Print lots of debugging statements",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="Be verbose",
        action="store_const", dest="loglevel", const=logging.INFO,
    )

    return parser.parse_args()

def main(args):
    
    args = parse_args(args)

    repos = get_repos(args.gname, args.base_api_url)
    
    with open('{}_github_repos.json'.format(args.gname), 'w') as f:
        json.dump({'git':repos}, f)

def get_repos(name, api_url = BASE_GITHUB_API_URL):
    logging.info("Getting {} GitHub organization git repos urls".format(name))
    query = "org:{}".format(name)
    page = 1
    repos = []

    r = requests.get(api_url+'/search/repositories?q={}&page={}'.format(query, page))
    items = r.json()['items']

    while len(items) > 0:
        time.sleep(5)
        for item in items:
            repos.append(item['clone_url'])
        page += 1
        r = requests.get(api_url+'/search/repositories?q={}&page={}'.format(query, page))
        try:
            items = r.json()['items']
        except:
            print('Error getting repos list')
            print(r.json())
    
    logging.info("Search process completed: {} repos found".format(len(repos)))
    return repos

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])