from django.shortcuts import render, redirect

import django_rq

from etl import repos

def update(request):

    repos.update_data()

    return redirect('/data/')