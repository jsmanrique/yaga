import logging

from django.utils import timezone

from perceval.backends.core.git import Git

from dateutil.parser import parse

from data.models import Project, Repository, Item

def update_git_data_repo(repo_url, project_name):
    repo_data, created = Repository.objects.get_or_create(url=repo_url, datasource='git')
    
    if created:
        logging.info('Creating {} repo'.format(repo_url))
    
    project = Project.objects.get(name=project_name)
    repo_data.projects.add(project)

    logging.info("Getting {} data".format(repo_data.url))
    get_git_data(repo_data)

    logging.info("Updating last data update for {} to: {}".format(repo_data.url, repo_data.last_update))
    repo_data.last_update = timezone.now()
    repo_data.save()

def get_git_data(repo):

    git_repo_name = repo.url.split('/')[-1]

    local_data_repository = Git(uri=repo.url, gitpath='/tmp/{}'.format(git_repo_name))

    items = []

    for commit in local_data_repository.fetch(from_date=repo.last_update):
        item = Item(
            item_id = commit['data']['commit'],
            author_name =  commit['data']['Author'],
            timestamp = parse(commit['data']['AuthorDate']),
            category = 'commit',
            repository = repo
        )
        items.append(item)

    Item.objects.bulk_create(items)