import time

from . import github

from scope.models import Project as Project, GitRepo, GitHubOrg, GitLabSet

def long_running_func(name, pid):

    project = Project.objects.get(pk=pid)

    repos = github.get_repos(name)
    for repo_url in repos:
        git_repo, created = GitRepo.objects.get_or_create(url=repo_url, project=project)