# YAGA (Yet Another Git Analyzer)

For anyone interested in building their own software development analytics tool, this is a pet project to showcase
how easy it is to start, and how hard it could be to produce valuable information without projects knowledge.
(**Tip**: Hire [Bitergia](https://bitergia.com) to save time (and \$\$\$))

YAGA (Yet Another {Git/GitHub/GitLab} Analyzer) is a pet project to play with
[Django](https://www.djangoproject.com/), [GrimoireLab/Perceval](https://github.com/chaoss/grimoirelab-perceval),
[Graphene](https://graphene-python.org/), and [Bokeh](https://docs.bokeh.org/en/latest/index.html) to produce some visualizations.

The aim is to build a simple tool that shall allow:

- To set up an analytics scope based on a list of repositories, GitHub organizations, or GitLab groups
- To gather data from those repositories
- To produce some basic charts based on, at least, [CHAOSS (Community Health Analytics for Open Source Software) Metrics](https://chaoss.community/metrics)
- To curate people information to avoid duplicate contributors or to add basic affiliation information
- To have a [GraphQL endpoint](https://graphql.org/) to query generated data

**Disclaimer**: This project is not intented to be ran in production, and I recommend to go for the
whole [GrimoireLab toolkit](https://chaoss.github.io/grimoirelab) or the [Bitergia Analytics Platform](https://bitergia.com/bitergia-analytics)
if you want a more tested, stable, and trusted solution. In any case, **you are more than welcome to contribute!**

# Requirements

- Python 3
- Docker and docker-compose

# How to run it

The app is built on [Django](https://www.djangoproject.com/), so I recommend to use a Python virtual environment to run it. The procedure would be as follows:

```
$ python3 -m venv .
$ source bin/activate
(yaga)$ pip install -r requirements.txt
(yaga)$ python manage.py makemigrations scope
(yaga)$ python manage.py makemigrations data
(yaga)$ python manage.py makemigrations
(yaga)$ python manage.py migrate
(yaga)$ python manage.py runserver
```

On a separate terminal, launch Redis and the [django-rq](https://github.com/rq/django-rq)
workers to allow tasks in background:

```
$ docker-compose up -d
...
$ source bin/activate
(yaga) $ python manage.py rqworker high default low
...
```

If everything has gone right, you should check `http://localhost:8000` in your browser to start using the app.

# How it works

![YaGA walkthrough](screenshots/yaga-walkthrough.gif)

## Adding projects and repositories

Click on `Add Project` button to add a new analysis project. Once you get it listed, click on it to `Add Data Source`. By now, it supports:

- single git repositories
- all the git repositories under a given GitHub organization or username
- all the git repositories under a given GitLab organization or username

![Projects set up](screenshots/projects-definition.gif)

Once you have added a GitLab or GitHub set, it would take some time to show the list of
repositories, because getting the list of repositories ran in background.

## Gathering data

In the Data section (`http://localhost:8000/data`), you can click in the Update button
to execute the data update in background.

Depending on the size of the project, it might take a while.

Once it's finished you'll be able to see some data ;-)

## Data consumption

There are 2 ways to consume the data:

- Through charts and metrics in `http://localhost:8000/data`
  ![YAGA data charts](screenshots/Screenshot_2020-05-29_YAGA_Data.png)

- Through data GraphQL endpoint in `http://localhost:8000/graphql`
  ![YAGA GraphQL](screenshots/Screenshot_2020-05-24_graphql.png)

## How to manage code contributors information

`TBD`

# License

GPL v3

# Logo

Git Logo by [Jason Long](https://twitter.com/jasonlong) is licensed under the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/). More info in [official Git logos website](https://git-scm.com/downloads/logos).
