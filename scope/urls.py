from django.urls import path
from scope import views

urlpatterns = [
    path('', views.scopepage, name='scopepage'),
    path('remove/<int:pid>', views.removeproject, name='removeproject'),
    path('<int:pid>/', views.projectview, name='projectview'),
    path('<int:pid>/remove/ghorg/<int:ghpid>', views.removeghorg, name='removeghorg'),
    path('<int:pid>/remove/repo/<int:repopid>', views.removerepo, name='removerepo'),
    path('<int:pid>/add/repo/', views.add_git_repo, name='add_git_repo'),
    path('<int:pid>/add/ghorg/', views.add_gh_org, name='add_gh_org'),
    path('<int:pid>/add/gitlab/', views.add_gitlab, name='add_gitlab'),
    path('<int:pid>/remove/gitlab/<int:glpid>', views.removegitlab, name='removegitlab'),
]