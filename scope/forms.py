from django import forms

from scope.models import Project, GitRepo, GitHubOrg, GitLabSet

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Project name'}),
        }

class GitRepoForm(forms.ModelForm):
    class Meta:
        model = GitRepo
        fields = ['url']

class GHOrgForm(forms.ModelForm):
    class Meta:
        model = GitHubOrg
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'GitHub organization or username'}),
        }

class GitLabForm(forms.ModelForm):
    class Meta:
        model = GitLabSet
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'GitLab group or username'}),
        }