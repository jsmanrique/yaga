from django.contrib import admin

# Register your models here.

from scope.models import Project, GitRepo, GitHubOrg

scope_models = [Project, GitRepo, GitHubOrg]

admin.site.register(scope_models)