from django.shortcuts import render, redirect

from scope.models import Project, GitRepo, GitHubOrg, GitLabSet

from scope.forms import ProjectForm, GitRepoForm, GHOrgForm, GitLabForm

import django_rq

from etl import repos

# Create your views here.
def scopepage(request):

    projects = Project.objects.all()

    if request.method == 'POST':
        project_form = ProjectForm(request.POST)
        if project_form.is_valid():
            project_form.save()
        return redirect('/')

    else:
        project_form = ProjectForm()

    return render (request, 'projects.html', {
        'projects': projects, 
        'pform': project_form,
        })

def removeproject(request, pid):

    Project.objects.get(pk=pid).delete()

    return redirect('/')

def projectview(request, pid):

    project = Project.objects.get(pk=pid)

    grepos = GitRepo.objects.filter(project__id=pid)

    ghorgs = GitHubOrg.objects.filter(project__id=pid)

    gitlabs = GitLabSet.objects.filter(project__id=pid)

    git_repo_form = GitRepoForm()

    gh_org_form = GHOrgForm()

    gitlab_form = GitLabForm()

    return render(request, 'project.html', {
        'project': project, 
        'grepos': grepos, 
        'ghorgs': ghorgs,
        'gitlabs': gitlabs, 
        'grepoform': git_repo_form,
        'ghorgform': gh_org_form,
        'gitlabform': gitlab_form,
        })

def removeghorg(request, pid, ghpid):
    
    GitHubOrg.objects.get(pk=ghpid).delete()

    return redirect ('/'+str(pid))

def removerepo(request, pid, repopid):
    
    GitRepo.objects.get(pk=repopid).delete()

    return redirect ('/'+str(pid))

def add_git_repo(request, pid):

    project = Project.objects.get(pk=pid)

    if request.method == 'POST':
        git_repo_form = GitRepoForm(request.POST)
        
        if git_repo_form.is_valid():
            repo = git_repo_form.save(commit = False)
            repo.project = project
            repo.save()
    
    return redirect ('/'+str(pid))

def add_gh_org(request, pid):

    project = Project.objects.get(pk=pid)

    if request.method == 'POST':
        gh_org_form = GHOrgForm(request.POST)
        
        if gh_org_form.is_valid():
            ghorg = gh_org_form.save(commit = False)
            ghorg.project = project
            ghorg.save()
            
            queue = django_rq.get_queue('high', is_async=True)
            queue.enqueue(repos.from_github, ghorg.name, pid)

    return redirect ('/'+str(pid))

def add_gitlab(request, pid):

    project = Project.objects.get(pk=pid)

    if request.method == 'POST':
        gitlab_form = GitLabForm(request.POST)
        
        if gitlab_form.is_valid():
            gitlab = gitlab_form.save(commit = False)
            gitlab.project = project
            gitlab.save()

            queue = django_rq.get_queue('high', is_async=True)
            queue.enqueue(repos.from_gitlab, gitlab.name, pid)
    
    return redirect ('/'+str(pid))

def removegitlab(request, pid, glpid):
    
    GitLabSet.objects.get(pk=glpid).delete()

    return redirect ('/'+str(pid))