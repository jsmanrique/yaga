from django.shortcuts import render
from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool


from data.models import Project, Repository, Item

import pandas as pd

# Create your views here.

def homepage(request):

    #Filters
    project_id = request.GET.get('pid')
    repo_id = request.GET.get('rid')

    #Data
    projects = []
    repositories = []
    project = None
    repository = None
    
    if project_id:
        project = Project.objects.get(pk=project_id)
        repositories = Repository.objects.filter(projects__id=project_id)
        items = Item.objects.filter(repository__projects__id=project_id)
    elif repo_id:
        repository = Repository.objects.get(pk=repo_id)
        projects = Repository.objects.get(pk=repo_id).projects.all()
        items = Item.objects.filter(repository__id=repo_id)
    else:
        projects = Project.objects.all()
        repositories = Repository.objects.all()
        items = Item.objects.all()
    print(projects)

    commits_df = pd.DataFrame.from_records(items.values())

    commits_df['timestamp'] = pd.to_datetime(commits_df['timestamp'])

    grouped = commits_df.groupby(pd.Grouper(key='timestamp', freq='M'))['item_id'].nunique()

    source = ColumnDataSource(pd.DataFrame(grouped))

    #Setup chart
    plot = figure(
        title = 'Test',
        x_axis_type='datetime',
        x_axis_label = 'Time',
        y_axis_label = 'Items',
        #sizing_mode = 'stretch_both',
        height = 400,
        sizing_mode = 'stretch_width',
        tools = 'pan,box_zoom,wheel_zoom,save, reset'
        )
    
    #Plot line
    plot.line(x='timestamp', y='item_id', source=source, line_width = 2)

    hover = HoverTool()
    hover.tooltips = [
        ('Commits', '@item_id'),
        ('Date', '@timestamp{%F}')
    ]
    hover.formatters = {
        '@timestamp': 'datetime'
    }
    hover.mode = 'vline'
    plot.add_tools(hover)

    #Store components
    script, div = components(plot)

    #Return to django page
    return render(request, 'homepage.html', {
        'projects': projects, 
        'repositories': repositories,
        'project': project, 
        'repository': repository, 
        'script': script, 
        'div': div,
        })