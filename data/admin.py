from django.contrib import admin

# Register your models here.

from .models import Project, Repository, Item

data_models = [Project, Repository, Item]

admin.site.register(data_models)